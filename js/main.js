// When the user scrolls down 80px from the top of the document, resize the navbar's padding and the logo's font size
window.onscroll = function() {scrollFunction();};


function scrollFunction() {
  if (document.body.scrollTop > 40 || document.documentElement.scrollTop > 40) {
    document.getElementById('eld-menu').classList.add('eld-header-light');
    // document.getElementById('eld-menu').classList.remove('navbar-dark');

    document.getElementById('eld-logo').src='images/logo-blue.svg';
    document.getElementById('eld-logo').width = '180';
  } else {
    document.getElementById('eld-menu').classList.remove('eld-header-light');
    // document.getElementById('eld-menu').classList.add('navbar-dark');

    document.getElementById('eld-logo').src='images/logo-white.svg';
    document.getElementById('eld-logo').width = '220';
  }
}


//Function to mark an item from a section width a css class.
function menuOnScroll(mySection, myMenu, myClass) {
  $(window).scroll(function(){
    var elScroll = $(window).scrollTop();
    $(mySection).each(function(i){
      if ($(this).offset().top <= elScroll) {
        $(myMenu).removeClass(myClass);
        $(myMenu).eq(i).addClass(myClass);
      }
    });
  });
}
//Call function.
//First parameter will be the section that we want to go.
//Second will be the item that will change his css.
//Third will be the class css to add to the item( Our second parameter) Is IMPORTAT to note that we must to skip the dot of our class.
menuOnScroll('eld-to-go','eld-main-nav eld-link-scroll', 'inSection');

//Function to animate the scroll when click on a menu item.
//IMPORTANT. This function is only use for animate the scroll, you could skip it if you want.
function scrollToAnyPoint (navItem) {
  var getAttr;
  $(navItem).click(function(e){
    e.preventDefault();
    getAttr = $(this).attr('href');
    var toSection = $(getAttr).offset().top;
    $('html, body').animate({scrollTop:toSection}, 1000);
  });
}
//Call Function
scrollToAnyPoint('.eld-main-nav .eld-link-scroll');


$('a[href=\'#home-btn\']').click(function() {
  $('html, body').animate({ scrollTop: 0 }, '1000');
  return false;
});

$('a[href=\'#about\']').click(function() {
  var toElement = $('#item2').offset().top;
  $('html, body').animate({ scrollTop: toElement }, '1000');
  return false;
});

// close menu on link click
$('#eld-menu ul  a').click(function() {
  $('#eld-menu').removeClass('nav-open');
});

// accordion
$('.eld-accordion .eld-accordion-item .eld-title-holder').click(function() {
  $('.eld-accordion .eld-accordion-item').removeClass('eld-active');
  $(this).parent('.eld-accordion .eld-accordion-item').addClass('eld-active');
  var newIndex = $('.eld-accordion-item.eld-active').index();
  $('.eld-accordion-img img').addClass('eld-none');
  $('.eld-accordion-img img:eq(' + newIndex + ')').removeClass('eld-none');
});
