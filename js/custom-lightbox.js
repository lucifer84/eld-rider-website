/// custom lightbox
$(document).ready(function() {
    $('.eld-lightbox-item').click(function() {
        var title = $(this).attr('alt');
        var imgPath = $(this).attr('src');

        $('#eld-lightbox .eld-lightbox-caption h5').text(title);
        $('#eld-lightbox .eld-lightbox-img-holder img').attr('src', imgPath);
        $('#eld-lightbox .eld-lightbox-img-holder img').attr('alt', title);

        $('#eld-lightbox').removeClass('eld-hide');
    });

    $('#eld-lightbox .eld-close').click(function() {
        $('#eld-lightbox').addClass('eld-hide');
    });

    $('#eld-lightbox .eld-lightbox-mask').click(function() {
        $('#eld-lightbox').addClass('eld-hide');
    });
});
